# Full-Stack-Server on TS code basis

This is a combination of a `common`, `NestJS` and `React` project. The 3 projects work together to give you the possibility to create re-usable code for both frontend and server code.

The React frontend creates a proxy to the server, so if you run the app in dev or debug mode make sure to call the port of the react server (default 3001) so your frontend gets refreshed on making changes.

### NPM scripts

In the global `package.json` are scripts that help you install and run the sub-projects without having to navigate into each of them manually.

| Command | Action |
| --- | --- |
| `build:frontend` | Builds the frontend and puts it into the `build` dir |
| `install:[project]` | Calls `npm install` inside the specified project (`install:all` calls this for each project, including the parent one) |
| `start` | Calls `install:all` and runs server and frontend concurrently |
| `start:frontend` | Starts the React dev server (default port 3001) |
| `start:server:[mode]` | Starts the server in the given mode (`dev`, `debug`, `prod`). Running in `prod` mode also calls `install:all` and `build:frontend`. |

### Pre-Installed packages

These packages come pre-installed with this server (besides the default ones included in `NestJS` and `React`)

- `axios` (server, frontend)
- `npm-run-all` (parent)
- `sass` (frontend)

### Usage

Before you do _anything_, please call `npm run install:all` so you have all needed dependencies installed in all projects.

You have some options to start this server the intended way. _Make sure to use the frontend port (default 3001) while in dev mode! Requests are automatically proxied to the dev server._

#### Using `npm run start`

This automatically installs all dependencies, builds the common package and starts server and frontend concurrently using `npm-run-all`. Since both use the same shell, logs might overwrite each other.

#### Starting server and frontend manually

You can run `npm run start:server:dev` and `npm run start:frontend` in different shells so you do not loose your logs and you have better overview. _Make sure to use `npm run install:all` before you start them tho, so you have all needed dependencies installed!_

### Tipps

Consider excluding some files or folders from your view for this project, so you do not loose focus of important files and folders. In VSCode, this can be done by adding this to your `settings.json` inside an `.vscode` folder:

```json
{
    "files.exclude": {
        "**/build": true,
        "**/dist": true,
        "**/node_modules": true,
        "**/*.d.ts": true,
        "**/*.js": true
    }
}
```

### What you can do to help us

If you see anything that is missing or should be added (packages added by default, etc) please let us know!

# Have Fun!